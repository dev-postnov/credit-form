$(function() {

$('.page-title').addClass('active');

 $('.form__input').focusin(function() {
  var placeholder = $(this).siblings('.form__placeholder');
  placeholder.addClass('active');
 });

 $('.form__input').keyup(function(){setWidthProgress()});

$('.form__input').focusout(function() {
  var placeholder = $(this).siblings('.form__placeholder');

  if ($(this).val() == ''){
    $(this).addClass('invalid');
    placeholder.removeClass('active').addClass('invalid')
  } else{
    $(this).addClass('valid')
    placeholder.addClass('valid');
  }

  setWidthProgress()
});


// Steps

var currentStep = $('#current-step').closest('.block-form').find('.form:visible').attr('data-step');
var allSteps = $('#all-steps').closest('.block-form').find('.form').length;

$('#current-step').text(currentStep);
$('#all-steps').text(allSteps);




//Button Next

$('.js-next-step').click(function(e) {
  e.preventDefault();
  var inputsValid = $(this).parent().find('input.valid'),
      inputs = $(this).parent().find('input');


   if (inputsValid.length < inputs.length){
    $(this).next().fadeIn(300);
   } else {

       var currentStep = $(this).parent('.form').attr('data-step');
       currentStep++;

       if (currentStep == allSteps) {
          $('.button-step').text('Отправить заявку');
          $('#current-step').text(allSteps);
          $(this).closest('.form').hide();
          $('.form[data-step='+ +currentStep+']').fadeIn(500);

          $('.button-step').click(function() {
            $(this).parent('form').submit();
          });
       } else{
          $('#current-step').text(currentStep);

       }

   }







 });











  function setWidthProgress(){
    var percent = $('#percent'),
        line = $('.block-form__line'),
        inputActiveAmount = $('.form__placeholder.active').length,
        inputAllAmount = $('.form__placeholder').length;
        percentLine = (inputActiveAmount / inputAllAmount ) * 100;

        percent.text(parseInt(percentLine))
        line.css('width', percentLine+'%');

  }

});




